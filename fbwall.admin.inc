<?php

/**
 * @file
 * Admin page callbacks for the fbwall module.
 *
 * @ingroup fbwall
 */

/**
 * Form builder; Configure the appearance of the Facebook News Feed.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function fbwall_config_form($form, $form_state) {
  $form['settings'] = array(
      '#type'   => 'fieldset',
      '#title'  => t('Settings'),
  );

  $form['settings']['fbwall_page'] = array(
      '#type'           => 'textfield',
      '#title'          => t('Page ID'),
      '#maxlength'      => 128,
      '#size'           => 32,
      '#default_value'  => variable_get('fbwall_page', FBWALL_PAGE),
      '#description'    => t('The ID of your Facebook page.<br /> This may be an numeric ID or the name, if you have an vanity URL (e.g. allfacebook or 6087692633).'),
      '#required'       => true,
  );

  $form['settings']['fbwall_width'] = array(
      '#type'           => 'textfield',
      '#title'          => t('iFrame Width'),
      '#maxlength'      => 5,
      '#size'           => 5,
      '#default_value'  => variable_get('fbwall_width', FBWALL_WIDTH),
      '#description'    => t('The width in px of the iFrame on your page. Default is 458px (like on a fan page).'),
  );

  $form['settings']['fbwall_lang'] = array(
      '#type'           => 'radios',
      '#title'          => t('Language'),
      '#options'        => array('de' => t('german'), 'en' => t('english')),
      '#default_value'  => variable_get('fbwall_lang', FBWALL_LANG),
      '#description'    => t('Select a language.'),
  );

  $form['settings']['fbwall_filter'] = array(
      '#type'           => 'radios',
      '#title'          => t('Default Filter'),
      '#options'        => array(2 => t('Only Posts by Page'), 1 => t('Most Recent')),
      '#default_value'  => variable_get('fbwall_filter', FBWALL_FILTER),
      '#description'    => t('Choose the default Filter. You can switch between the filters easily in the Social Add-on.'),
  );

  return system_settings_form($form);
}

/**
 * Validate fbwall_config_form.
 */
function fbwall_config_form_validate($form, &$form_state) {
  if (empty($form_state['values']['fbwall_width']) ||
          !preg_match('/[[:digit:]]/', $form_state['values']['fbwall_width']) ||
          $form_state['values']['fbwall_width'] < FBWALL_WIDTH) {
    $form_state['values']['fbwall_width'] = FBWALL_WIDTH;
    drupal_set_message(t('Width automatically set to ' . FBWALL_WIDTH . 'px, which is the default and also the minimum value.'));
  }
}

/**
 * Provide a single block from the administration menu as a page.
 *
 * @return The output HTML.
 */
function fbwall_admin_menu_block_page() {
  $item = menu_get_item();
  if ($content = fbwall_admin_menu_block($item)) {
    $output = theme('admin_block_content', array('content' => $content));
  } else {
    $output = t('You do not have any administrative items.');
  }
  return $output;
}